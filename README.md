
Домашнее задание к лекции 1.1 «Введение в PHP»
Создать файл about.php.
Создать переменные и ввести в них: ваше имя, возраст, адрес электронной почты, город и краткий текст о себе.
Представить эту информацию в виде простого HTML-документа, используя шаблонную архитектуру языка PHP, и вывести результат (пример).
Разместить на хостинг в папку /me/.
Прислать PHP-код и ссылку на пример.