<?php
    $x = rand(0,100);
    echo "Введено число : $x \n";  // проверим что это число Фибоначчи...
    $var1 = 1;
    $var2 = 1;
    $var3 = null;

    for(;;)
	{ // цикл бесконечный...  
        if( $var1 > $x )
        {
            echo "Задуманное число $x НЕ входит в числовой ряд.\n";
			break;
        }
        else 
        {
            if ($x === $var1)
			{
                echo "Число $x входит в числовой ряд.\n";
				break;
            }
            else
            {
                $var3 = $var1;
                $var1 = $var1 + $var2;
                $var2 = $var3;
                //echo "$var1 $var2 $var3 \n"; // debug
            }
        }
	}
?>